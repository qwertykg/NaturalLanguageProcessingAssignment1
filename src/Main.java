import framework.containers.FrameContainer;
import framework.containers.LayoutType;
import framework.containers.PanelContainer;
import framework.controls.ControlType;
import framework.controls.ControlTypePosition;
import framework.controls.types.Control;
import framework.controls.types.TextAreaControl;
import framework.controls.types.TextFieldControl;
import startup.StartupChatBot;
import startup.StartupMinimumEditDistance;

public class Main 
{
	public static void main(String[] args)
	{
		FrameContainer frame = new FrameContainer(800, 600, "projectConfig");
		PanelContainer panel = new PanelContainer(800, 40, LayoutType.GridLayout);
		
		Control minimumEditDistanceButton = panel.addControl(ControlType.Button, ControlTypePosition.Left, "minimumEditDistance/minimumEditDistanceButtonDefinition");
		Control chatBotButton = panel.addControl(ControlType.Button, ControlTypePosition.Center, "chatBot/chatBotButtonDefinition");
		
		setupMinimumEditDistanceCallbackToButton(minimumEditDistanceButton);
		setupChatBotCallbackToButton(chatBotButton);
		
		frame.addPanelToFrame(panel, ControlTypePosition.Top);
	}
	
	static void setupMinimumEditDistanceCallbackToButton(Control minimumEditDistanceButton)
	{
		Runnable minimumEditDistanceDemonstration = () -> {
			StartupMinimumEditDistance minimumEditDistanceRunner = new StartupMinimumEditDistance();
			minimumEditDistanceRunner.setupUI();
		};
		
		minimumEditDistanceButton.addOnClickListener(minimumEditDistanceDemonstration);
	}
	 
	
	static void setupChatBotCallbackToButton(Control chatBotButton)
	{
		Runnable invokeChatBot = () -> {
			StartupChatBot chatBot = new StartupChatBot();
			chatBot.setupUI();
			chatBot.setupMessageService();
		};
		
		chatBotButton.addOnClickListener(invokeChatBot);
	}
}
