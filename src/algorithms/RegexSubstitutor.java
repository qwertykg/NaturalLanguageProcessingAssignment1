package algorithms;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexSubstitutor
{
	ArrayList<String> genericResponses = new ArrayList<String>();
	
	public RegexSubstitutor()
	{
		genericResponses.add("Do you have any other symptoms you would like to talk about?");
		genericResponses.add("Could you elaborate more about your illness please?");
		genericResponses.add("Is there anything I can do for you?");
		genericResponses.add("How long have you had your symptoms for?");
	}
	
	public String getResponse(String currentMessage)
	{
		if (isRequestingFirstMessage(currentMessage))
		{
			return getFirstMessage();
		}
		
		String substitution = findSubstitute(currentMessage);
		
		if (substitution != null) return substitution;
		
		return getGenericResponseForUnknownSubstitution();
	}
	
	private String getGenericResponseForUnknownSubstitution()
	{
	    int index = (int)(Math.random() * genericResponses.size());
		return genericResponses.get(index);
	}
	
	private String findSubstitute(String currentMessage)
	{
		String iAmRegex = "([Ii] am )(.*)";
		String matchIam = matchExpression(iAmRegex, currentMessage, 2);
		
		if(matchIam != null)
		{
			return performSubstitute("Why do you say that you are ", matchIam+"?");
		}
		
		String iFeelRegex = "([Ii] feel )(.*)";
		String matchIFeel = matchExpression(iFeelRegex, currentMessage, 2);
		
		if(matchIFeel != null)
		{
			return performSubstitute("Why do you feel ", matchIFeel+"? What are your symptoms?");
		}
		
		String mySymptomsRegex = "([Mm]y symptoms are|include )(.*)";
		String matchMySymptoms = matchExpression(mySymptomsRegex, currentMessage, 2);
		
		if(matchMySymptoms != null)
		{
			return performSubstitute("For how long have you had ", matchMySymptoms+" as symptoms?");
		}
		
		String iNeedRegex = "([Ii] need )(.*)";
		String matchIneed = matchExpression(iNeedRegex, currentMessage, 2);
		
		if(matchIneed != null)
		{
			return performSubstitute("Why do you need ", matchIneed+"?");
		}
		
		String iHurtRegex = "([Ii] hurt )(my )(.*)";
		String matchIhurt = matchExpression(iHurtRegex, currentMessage, 3);
		
		if(matchIhurt != null)
		{
			return performSubstitute("How did you hurt your ", matchIhurt+"?");
		}
		
		String iThinkRegex = "([Ii] think [Ii])(.*)";
		String matchIThink = matchExpression(iThinkRegex, currentMessage, 2);
		
		if(matchIThink != null)
		{
			return performSubstitute("Why do you think you ", matchIThink+"?");
		}
		
		return null;
	}
	
	private String performSubstitute(String partialResponse1, String partialResponse2)
	{
		return partialResponse1 + partialResponse2;
	}

	private String matchExpression(String regex, String currentMessage, int registerToUse)
	{
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(currentMessage);
		if (matcher.find())
		{
			return matcher.group(registerToUse);
		}
		return null;
	}
	
	private String getFirstMessage()
	{
		return "Hi, I'm Doctor Atlas, how can I be of service today?";
		
	}
	
	private boolean isRequestingFirstMessage(String currentMessage)
	{
		return currentMessage.isEmpty();
	}
	
}
