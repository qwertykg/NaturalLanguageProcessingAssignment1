package algorithms;

public class MinimumEditDistanceGenerator 
{
	int [][] distance;
	
	public int generate(String target, String source, int insertionCost, int deletionCost, int substitutionCost)
	{
		int targetLength = target.length();
		int sourceLength = source.length();
		
		distance = new int[targetLength + 1][sourceLength + 1];
		
		distance[0][0] = 0;
		
		for(int i = 1; i <= targetLength; i++)
		{
			distance[i][0] = distance[i-1][0] + insertionCost;
		}
		
		for(int j = 1; j <= targetLength; j++)
		{
			distance[0][j] = distance[0][j-1] + deletionCost;
		}
		
		for(int i = 1; i <= targetLength; i++)
		{
			for(int j = 1; j <= targetLength; j++)
			{
				int actualSubstitutionCost = substitutionCost;
				
				if(target.charAt(i-1) == source.charAt(j-1))
				{
					actualSubstitutionCost = 0;
				}
				
				int insertion = distance[i-1][j] + insertionCost;
				int deletion = distance[i][j-1] + deletionCost;
				int substitution = distance[i-1][j-1] + actualSubstitutionCost;
				
				distance[i][j] = getSmallestCost(insertion, deletion, substitution);
			}
		}
		
		return distance[targetLength][sourceLength];
	}
	
	int getSmallestCost(int insertion, int deletion, int substitution)
	{
		int smallerCost = Math.min(insertion, deletion);
		int smallestCost = Math.min(smallerCost, substitution);
		return smallestCost;
	}

	public String showMatrix() 
	{
		String result = "";
		
		for(int row = 0; row < distance.length; row++) {
		     for(int col = 0; col < distance[row].length; col++) {
		    	 result += " " + distance[row][col];
		     }
		     result += "\r\n";
		  }
	return result;
	}
}
