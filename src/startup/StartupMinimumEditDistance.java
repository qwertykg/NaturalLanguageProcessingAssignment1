package startup;

import javax.swing.JOptionPane;

import algorithms.MinimumEditDistanceGenerator;
import framework.containers.FrameContainer;
import framework.containers.LayoutType;
import framework.containers.PanelContainer;
import framework.controls.ControlType;
import framework.controls.ControlTypePosition;
import framework.controls.types.ButtonControl;
import framework.controls.types.Control;
import framework.controls.types.TextFieldControl;

public class StartupMinimumEditDistance 
{
	TextFieldControl firstWordTextField;
	TextFieldControl secondWordTextField;
	
	TextFieldControl insertionCostTextField;
	TextFieldControl deletionCostTextField;
	TextFieldControl substitutionCostTextField;
	
	ButtonControl generateButton;
	
	FrameContainer frame;
	
	public void setupUI()	
	{		
		frame = new FrameContainer(800, 800, "minimumEditDistance/minimumEditDistanceConfig");
		
		PanelContainer panel = new PanelContainer(800, 40, LayoutType.BorderLayout);
		
		panel.addPanelToPanel(createLeftPanel(), ControlTypePosition.Left);
		panel.addPanelToPanel(createRightPanel(), ControlTypePosition.Right);
		panel.addPanelToPanel(createBottomPanel(), ControlTypePosition.Bottom);
		
		setupGenerationCallbackToButton(generateButton);
		
		frame.addPanelToFrame(panel, ControlTypePosition.Center);	
	}	
	
	PanelContainer createLeftPanel()
	{
		PanelContainer leftPanel = new PanelContainer(400, 40, LayoutType.FlowLayout);

		leftPanel.addControl(ControlType.Label, ControlTypePosition.Left, "minimumEditDistance/firstWordTextDefinition");
		leftPanel.addControl(ControlType.Label, ControlTypePosition.Left, "minimumEditDistance/secondWordTextDefinition");
		
		leftPanel.addControl(ControlType.Label, ControlTypePosition.Left, "minimumEditDistance/insertionCostTextDefinition");
		leftPanel.addControl(ControlType.Label, ControlTypePosition.Left, "minimumEditDistance/deletionCostTextDefinition");
		leftPanel.addControl(ControlType.Label, ControlTypePosition.Left, "minimumEditDistance/substitutionCostTextDefinition");

		return leftPanel;
	}

	PanelContainer createRightPanel()
	{
		PanelContainer rightPanel = new PanelContainer(400, 40, LayoutType.FlowLayout);

		firstWordTextField = (TextFieldControl)rightPanel.addControl(ControlType.TextField, ControlTypePosition.Right, "minimumEditDistance/firstWordTextFieldDefinition");
		secondWordTextField = (TextFieldControl)rightPanel.addControl(ControlType.TextField, ControlTypePosition.Right, "minimumEditDistance/secondWordTextFieldDefinition");
		
		insertionCostTextField = (TextFieldControl)rightPanel.addControl(ControlType.TextField, ControlTypePosition.Right, "minimumEditDistance/insertionCostTextFieldDefinition");
		deletionCostTextField = (TextFieldControl)rightPanel.addControl(ControlType.TextField, ControlTypePosition.Right, "minimumEditDistance/deletionCostTextFieldDefinition");
		substitutionCostTextField = (TextFieldControl)rightPanel.addControl(ControlType.TextField, ControlTypePosition.Right, "minimumEditDistance/substitutionCostTextFieldDefinition");
		
		return rightPanel;
	}
	
	PanelContainer createBottomPanel()
	{
		PanelContainer bottomPanel = new PanelContainer(400, 40, LayoutType.FlowLayout);

		generateButton = (ButtonControl)bottomPanel.addControl(ControlType.Button, ControlTypePosition.Bottom, "minimumEditDistance/generateMinEditDistanceButtonDefinition");
		
		return bottomPanel;
	}
	
	 void setupGenerationCallbackToButton(Control minimumEditDistanceButton)
	{			
		Runnable generationRunner = () -> {

			String firstWord = firstWordTextField.getText();
			String secondWord = secondWordTextField.getText();;
				
			int insertionCost = 0;
			int deletionCost = 0;
			int substitutionCost = 0;
			
			try
			{
				insertionCost = Integer.parseInt(insertionCostTextField.getText());
				deletionCost = Integer.parseInt(deletionCostTextField.getText());
				substitutionCost = Integer.parseInt(substitutionCostTextField.getText());
		
				MinimumEditDistanceGenerator generator = new MinimumEditDistanceGenerator();
				int response = generator.generate(firstWord, secondWord, insertionCost, deletionCost, substitutionCost);
				JOptionPane.showMessageDialog(frame.frame, "The Minimum edit distance cost is " + response);
				
				int dialogResult = JOptionPane.showConfirmDialog (null, "Would you like to see the distance matrix?", "Distance Matrix", JOptionPane.YES_NO_OPTION);
			
				if(dialogResult == JOptionPane.YES_OPTION)
				{
					JOptionPane.showMessageDialog (null, generator.showMatrix(), "Distance Matrix", JOptionPane.PLAIN_MESSAGE); 
				}
			} 
			catch(NumberFormatException e)
			{
				JOptionPane.showMessageDialog(frame.frame, "Please use numeric values for costs");
			}
		};
		
		minimumEditDistanceButton.addOnClickListener(generationRunner);
	}
	 

}
