package startup;

import framework.containers.FrameContainer;
import framework.containers.LayoutType;
import framework.containers.PanelContainer;
import framework.controls.ControlType;
import framework.controls.ControlTypePosition;
import framework.controls.types.Control;
import services.MessageService;

public class StartupChatBot
{
	Control textArea;
	Control button;
	Control textField;
	
	MessageService messageService;
		
	public void setupUI()
	{
		FrameContainer frame = new FrameContainer(800, 800, "chatBot/chatBotConfig");
		PanelContainer panel = new PanelContainer(800, 40, LayoutType.FlowLayout);
		
		textArea = frame.addControl(ControlType.TextArea, ControlTypePosition.Center);
		textField = panel.addControl(ControlType.TextField, ControlTypePosition.Bottom, "chatBot/textFieldDefinition");
		button = panel.addControl(ControlType.Button, ControlTypePosition.Bottom, "chatBot/sendMessageButtonDefinition");
		
		frame.addPanelToFrame(panel, ControlTypePosition.Bottom);
	}
	
	public void setupMessageService()
	{
		messageService = new MessageService();
		messageService.setupMessageListener(textField, button, textArea);
	}
}
