package services;

public class StringFormattingService
{
	public String getFormattedUserMessageString(String userMessage)
	{
		return "USER: " + userMessage;
	}
	
	public String getFormattedBotMessageString(String botMessage)
	{
		return "BOT: " + botMessage;
	}
}
