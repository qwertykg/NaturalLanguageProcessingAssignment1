package services;

import algorithms.RegexSubstitutor;
import framework.controls.types.Control;
import framework.controls.types.TextAreaControl;
import framework.controls.types.TextFieldControl;

public class MessageService
{
	StringFormattingService formatter;
	
	public void setupMessageListener(Control textField, Control button, Control textArea)
	{
		formatter = new StringFormattingService();
		
		Runnable onSendMessageButtonClicked = () ->
		{
			sendMessage((TextFieldControl) textField, (TextAreaControl) textArea);
		};
		
		button.addOnClickListener(onSendMessageButtonClicked);
		
		getFirstMessage((TextAreaControl) textArea);
	}
	
	public void getFirstMessage(TextAreaControl textArea)
	{
		String firstMessage = requestResponse("");
		textArea.setText(formatter.getFormattedBotMessageString(firstMessage));
	}
	
	private String requestResponse(String currentMessage)
	{
		return new RegexSubstitutor().getResponse(currentMessage);
	}
	
	private void sendMessage(TextFieldControl textField, TextAreaControl textAarea)
	{
		String currentMessage = textField.getText();
		textField.setText("");
		
		if (currentMessage != null && currentMessage.length() > 0)
		{
			textAarea.addNewLine(formatter.getFormattedUserMessageString(currentMessage));
			
			String response = requestResponse(currentMessage);
			
			textAarea.addNewLine(formatter.getFormattedBotMessageString(response));
		}
	}
}
