package framework.resourceLoader;

import java.io.FileReader;
import java.io.IOException;
import javax.swing.ImageIcon;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ResourceLoader
{
	public ImageIcon getImageAsImageIcon(String image)
	{
		return new ImageIcon("resources/" + image + ".png");
	}
	
	public JSONObject getJsonObjectFromFile(String filename)
	{
		try
		{
			JSONParser parser = new JSONParser();
			Object obj;
			
			obj = parser.parse(new FileReader("src/config/" + filename + ".json"));
			
			return (JSONObject) obj;
		} 
		catch (IOException | ParseException e)
		{
			System.out.println("File " + filename + ".json could not be found or parsed. Please check if the file exists in the src/config directory");
			e.printStackTrace();
		}
		
		return null;
	}
}