package framework.controls;

public enum ControlTypePosition
{
	Left,
	Right,
	Bottom,
	Top,
	Center
}
