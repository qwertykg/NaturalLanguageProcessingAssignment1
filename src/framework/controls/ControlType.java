package framework.controls;

public enum ControlType
{
	Panel,
	TextArea,
	Button,
	TextField,
	Label
}
