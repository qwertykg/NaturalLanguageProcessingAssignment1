package framework.controls.types;

import javax.swing.JLabel;
import javax.swing.JTextArea;

import org.json.simple.JSONObject;

public class LabelControl extends Control
{
	JLabel label;
	
	public LabelControl(JSONObject controlDefinition)
	{
		super(new JLabel());
		
		label = (JLabel) (renderable);
				
		if (controlDefinition != null)
		{
			setProperties(controlDefinition);
		}
	}
	
	protected void setProperties(JSONObject controlDefinition)
	{
		super.setProperties(controlDefinition);
		
		if (hasField(controlDefinition, "text"))
		{
			String text = (String) getField(controlDefinition, "text");
			setText(text);
		}
	}
	
	public String getText()
	{
		return label.getText();
	}
	
	public void setText(String text)
	{
		label.setText(text);
	}
}
