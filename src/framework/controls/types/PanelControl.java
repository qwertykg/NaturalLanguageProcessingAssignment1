package framework.controls.types;

import javax.swing.JPanel;
import org.json.simple.JSONObject;

public class PanelControl extends Control
{
	JPanel panel;
	
	public PanelControl(JSONObject controlDefinition)
	{
		super(new JPanel());
		
		panel = (JPanel)(renderable);
		
		if (controlDefinition != null)
		{
			setProperties(controlDefinition);
		}
	}
}
