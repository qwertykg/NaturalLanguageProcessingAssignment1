package framework.controls.types;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import org.json.simple.JSONObject;

public abstract class Control implements MouseListener
{
	public Component renderable;
	
	Runnable onClick;
	Runnable onPressed;
	Runnable onReleased;
	Runnable onEntered;
	Runnable onExited;
	
	public Control(Component component)
	{
		renderable = component;
		renderable.addMouseListener(this);
	}
	
	protected boolean hasField(JSONObject controlDefinition, String field)
	{
		return controlDefinition.containsKey(field);
	}
	
	protected Object getField(JSONObject controlDefinition, String field)
	{
		return controlDefinition.get(field);
	}
	
	protected void setProperties(JSONObject controlDefinition)
	{
		if (hasField(controlDefinition, "name"))
		{
			setName((String) getField(controlDefinition, "name"));
		}
		
		if (hasField(controlDefinition, "width") || hasField(controlDefinition, "height"))
		{
			int width = ((Long) getField(controlDefinition, "width")).intValue();
			int height = ((Long) getField(controlDefinition, "height")).intValue();
			
			renderable.setPreferredSize(new Dimension(width, height));
		}
	}
	
	public String getName()
	{
		return renderable.getName();
	}
	
	public void setName(String name)
	{
		renderable.setName(name);
	}
	
	public int getWidth()
	{
		return renderable.getWidth();
	}
	
	public int getHeight()
	{
		return renderable.getHeight();
	}
	
	public void addOnClickListener(Runnable callBack)
	{
		onClick = callBack;
	}
	
	public void addOnPressedListener(Runnable callBack)
	{
		onPressed = callBack;
	}
	
	public void addOnReleaseListener(Runnable callBack)
	{
		onReleased = callBack;
	}
	
	public void addOnEnteredListener(Runnable callBack)
	{
		onEntered = callBack;
	}
	
	public void addOnExitedListener(Runnable callBack)
	{
		onExited = callBack;
	}
	
	public void mouseClicked(MouseEvent e)
	{
		if (onClick != null)
		{
			onClick.run();
		}
	}
	
	public void mousePressed(MouseEvent e)
	{
		if (onPressed != null)
		{
			onPressed.run();
		}
	}
	
	public void mouseReleased(MouseEvent e)
	{
		if (onReleased != null)
		{
			onReleased.run();
		}
	}
	
	public void mouseEntered(MouseEvent e)
	{
		if (onEntered != null)
		{
			onEntered.run();
		}
	}
	
	public void mouseExited(MouseEvent e)
	{
		if (onExited != null)
		{
			onExited.run();
		}
		
	}
}
