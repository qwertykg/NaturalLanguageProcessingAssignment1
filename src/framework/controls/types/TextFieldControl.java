package framework.controls.types;

import javax.swing.JTextField;
import org.json.simple.JSONObject;

public class TextFieldControl extends Control
{
	JTextField textField;
	
	public TextFieldControl(JSONObject controlDefinition)
	{
		super(new JTextField());
		
		textField = (JTextField) (renderable);
		
		if (controlDefinition != null)
		{
			setProperties(controlDefinition);
		}
	}
	
	public String getText()
	{
		return textField.getText();
	}
	
	public void setText(String text)
	{
		textField.setText(text);
	}
}
