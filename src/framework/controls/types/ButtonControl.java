package framework.controls.types;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import org.json.simple.JSONObject;

import framework.resourceLoader.ResourceLoader;

public class ButtonControl extends Control
{
	JButton button;
	
	public ButtonControl(JSONObject controlDefinition)
	{
		super(new JButton());
		
		button = (JButton)(renderable);
		
		if (controlDefinition != null)
		{ 
			setProperties(controlDefinition);
		}
	}
	
	protected void setProperties(JSONObject controlDefinition)
	{
		super.setProperties(controlDefinition);
		
		if (hasField(controlDefinition, "image"))
		{
			String imageName = (String) getField(controlDefinition, "image");
			setImage(imageName);
		}
	}
	
	public void setImage(String imageName)
	{
		ImageIcon image = new ResourceLoader().getImageAsImageIcon(imageName);
		
		button.setIcon(image);;
	}
}