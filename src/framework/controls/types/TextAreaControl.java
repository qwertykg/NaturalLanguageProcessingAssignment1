package framework.controls.types;

import javax.swing.JTextArea;
import org.json.simple.JSONObject;

public class TextAreaControl extends Control
{
	JTextArea textArea;
	
	public TextAreaControl(JSONObject controlDefinition)
	{
		super(new JTextArea());
		
		textArea = (JTextArea) (renderable);
		
		textArea.setEditable(false);
		
		if (controlDefinition != null)
		{
			setProperties(controlDefinition);
		}
	}
	
	public String getText()
	{
		return textArea.getText();
	}
	
	public void setText(String text)
	{
		textArea.setText(text);
	}
	
	public void addNewLine(String text)
	{
		String currentText = getText();
		textArea.setText(currentText + "\n" + text);
	}
}
