package framework.containers;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JPanel;
import framework.controls.ControlType;
import framework.controls.ControlTypePosition;
import framework.controls.types.Control;

public class PanelContainer extends GenericContainer
{
	public JPanel panel;
	
	boolean alwaysCentered;
	
	public PanelContainer(int width, int height, LayoutType layout)
	{
		panel = new JPanel();
		setLayoutOnContainer(panel, layout);
		panel.setVisible(true);
		panel.setPreferredSize(new Dimension(width, height));
	}
	
	public void resize(int width, int height)
	{
		panel.setSize(width, height);
	}
	
	public Control addControl(ControlType type, ControlTypePosition controlTypePosition, String controlDefinitionFileName)
	{
		return addControlToContainer(panel, type, controlTypePosition, controlDefinitionFileName);
	}
	
	public void addPanelToPanel(PanelContainer panel, ControlTypePosition position)
	{
		this.panel.add(panel.panel, mapControlTypePositionToLayoutPosition(position));
		this.panel.revalidate();
	}
}
