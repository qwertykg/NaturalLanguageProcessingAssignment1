package framework.containers;

public enum LayoutType 
{
	BorderLayout("BorderLayout"),
	GridLayout("GridLayout"),
	FlowLayout("FlowLayout");
	
	private String layoutType;

	LayoutType(String layoutType) {
		this.layoutType = layoutType;
	}

	public String getLayoutType() {
		return layoutType;
	}
}
