package framework.containers;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import org.json.simple.JSONObject;
import framework.controls.ControlType;
import framework.controls.ControlTypePosition;
import framework.controls.types.ButtonControl;
import framework.controls.types.Control;
import framework.controls.types.LabelControl;
import framework.controls.types.PanelControl;
import framework.controls.types.TextAreaControl;
import framework.controls.types.TextFieldControl;
import framework.resourceLoader.ResourceLoader;

public abstract class GenericContainer
{
	protected ResourceLoader loader;
	
	public GenericContainer()
	{
		loader = new ResourceLoader();
	}
	
	protected Control addControlToContainer(Container container, ControlType type, ControlTypePosition controlTypePosition, String controlDefinitionFileName)
	{
		Control control = null;
		JSONObject controlDefinition = null;
		
		if (controlDefinitionFileName != null)
		{
			controlDefinition = loader.getJsonObjectFromFile(controlDefinitionFileName);
		}
		switch (type)
		{
			case Panel:
			control = new PanelControl(controlDefinition);
			container.add(control.renderable, mapControlTypePositionToLayoutPosition(controlTypePosition));
			break;
			
			case TextArea:
			control = new TextAreaControl(controlDefinition);
			container.add(control.renderable, mapControlTypePositionToLayoutPosition(controlTypePosition));
			break;
			
			case Button:
			control = new ButtonControl(controlDefinition);
			container.add(control.renderable, mapControlTypePositionToLayoutPosition(controlTypePosition));
			break;
			
			case TextField:
			control = new TextFieldControl(controlDefinition);
			container.add(control.renderable, mapControlTypePositionToLayoutPosition(controlTypePosition));
			break;
			
			case Label:
			control = new LabelControl(controlDefinition);
			container.add(control.renderable, mapControlTypePositionToLayoutPosition(controlTypePosition));
			break;
			
			default:
			JOptionPane.showMessageDialog(container, "Control Type " + type + " not supported yet");
			break;
		}
		
		container.revalidate();
		
		return control;
	}
	
	public abstract Control addControl(ControlType type, ControlTypePosition controlTypePosition, String controlDefinitionFileName);
	
	public Control addControl(ControlType type, ControlTypePosition controlTypePosition)
	{
		return addControl(type, controlTypePosition, null);
	}
	
	public abstract void resize(int width, int height);
	
	protected String mapControlTypePositionToLayoutPosition(ControlTypePosition position)
	{
		switch (position)
		{
			case Top:
			return BorderLayout.PAGE_START;
			
			case Bottom:
			return BorderLayout.PAGE_END;
			
			case Left:
			return BorderLayout.LINE_START;
			
			case Right:
			return BorderLayout.LINE_END;
			
			case Center:
			return BorderLayout.CENTER;
		}
		
		return null;
	}
	
	protected void setLayoutOnContainer(Container container, LayoutType layout) 
	{		
		switch(layout)
		{
		case BorderLayout:
			container.setLayout(new BorderLayout());
			break;
		case FlowLayout:
			container.setLayout(new FlowLayout());
			break;
		case GridLayout:
			container.setLayout(new GridLayout(0, 1));
			break;
		}
	}
}
