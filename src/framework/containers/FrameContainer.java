package framework.containers;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.JFrame;
import org.json.simple.JSONObject;
import framework.controls.ControlType;
import framework.controls.ControlTypePosition;
import framework.controls.types.Control;

public class FrameContainer extends GenericContainer
{
	public JFrame frame;
	
	boolean alwaysCentered;
	
	public FrameContainer(int width, int height, String configFileName)
	{
		JSONObject projectConfig = loader.getJsonObjectFromFile(configFileName);
		
		String projecTitle = (String) projectConfig.get("projectTitle");
		String layoutManager = (String) projectConfig.get("layoutManager");
		
		int initialXposition = ((Long) projectConfig.get("initialXposition")).intValue();
		int initialYposition = ((Long) projectConfig.get("initialYposition")).intValue();
		
		boolean exitOnClose = (boolean) projectConfig.get("exitOnClose");
		boolean resizable = (boolean) projectConfig.get("resizable");
		alwaysCentered = (boolean) projectConfig.get("alwaysCentered");
			
		frame = new JFrame(projecTitle);
		
		frame.setBounds(initialXposition, initialYposition, width, height);
		
		if (exitOnClose)
		{
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		
		if (alwaysCentered)
		{
			frame.setLocationRelativeTo(null);
		}
		
		frame.setVisible(true);
		frame.setResizable(resizable);
		
		if(layoutManager == null)
		{
			frame.setLayout(new BorderLayout());			
		}
		else
		{
			setLayoutOnContainer((Container)frame, LayoutType.valueOf(layoutManager));
		}
		
		frame.pack();
		frame.setMinimumSize(new Dimension(width, height));
	}
	
	public void resize(int width, int height)
	{
		frame.setMinimumSize(new Dimension(width, height));
		frame.setSize(width, height);
		
		if (alwaysCentered)
		{
			frame.setLocationRelativeTo(null);
		}
	}
	
	public Control addControl(ControlType type, ControlTypePosition controlTypePosition,
	        String controlDefinitionFileName)
	{
		return addControlToContainer(frame, type, controlTypePosition, controlDefinitionFileName);
	}
	
	public void addPanelToFrame(PanelContainer panel, ControlTypePosition position)
	{
		frame.add(panel.panel, mapControlTypePositionToLayoutPosition(position));
		frame.revalidate();
	}
}
